﻿using CustomerAPI.Common;
using System;
using System.Collections.Generic;

namespace CustomerAPI.Interfaces
{
    public interface ICustomerDatabaseManager
    {
        Guid CreateCustomer(Customer customer);
        List<Customer> GetCustomers();
        void UpdateCustomer(Customer customer);
        void DeleteCustomer(Guid Id);
        Customer GetCustomer(Guid Id);
    }
}
