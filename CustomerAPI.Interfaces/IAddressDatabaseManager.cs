﻿using CustomerAPI.Common;
using System;
using System.Collections.Generic;

namespace CustomerAPI.Interfaces
{
    public interface IAddressDatabaseManager
    {
        Guid CreateAddress(Customer customer, Address address);
        Address GetAddress(Guid id);
        List<Address> GetAddresses();
        void DeleteAddress(Guid id);
    }
}
