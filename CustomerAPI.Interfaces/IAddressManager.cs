﻿using CustomerAPI.Common;
using System;
using System.Collections.Generic;

namespace CustomerAPI.Interfaces
{
    public interface IAddressManager
    {
        public Guid CreateAddress(Address address, Guid CustomerId);
        public List<Address> GetAllAddresses();
        public Address GetAddressById(Guid id);
        public void DeleteAddress(Guid id);
    }
}
