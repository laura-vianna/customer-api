﻿using CustomerAPI.Common;
using System;
using System.Collections.Generic;

namespace CustomerAPI.Interfaces
{
    public interface ICustomerManager
    {
        public Guid CreateCustomer(Customer customer);
        public Customer GetCustomerById(Guid id);
        public List<Customer> GetAllCustomers();
        void DeleteCustomer(Guid id);
    }
}
