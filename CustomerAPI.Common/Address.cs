﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace CustomerAPI.Common
{
    public class Address
    {
        public Guid AddressId { get; set; }

        [Required]
        [StringLength(80, MinimumLength = 1)]
        public string AddressLine1 { get; set; }

        [StringLength(80)]
        public string AddressLine2 { get; set; }

        [Required]
        [StringLength(50, MinimumLength = 1)]
        public string Town { get; set; }

        [StringLength(50)]
        public string County { get; set; }

        [Required]
        [StringLength(10, MinimumLength = 1)]
        public string Postcode { get; set; }

        public string Country { get; set; }

        [JsonIgnore]
        public virtual Customer Customer { get; set; }

        public Guid CustomerId { get; set; }

        public Address()
        {
            AddressId = Guid.NewGuid();
        }

        public Address(string AddressLine1, string AddressLine2, string Town, string County, string Postcode) : this()
        {
            this.AddressLine1 = AddressLine1;
            this.AddressLine2 = AddressLine2;
            this.Town = Town;
            this.County = County;
            this.Postcode = Postcode;
            Country = Constants.DefaultCountry;
        }

        public Address(string AddressLine1, string AddressLine2, string Town, string County, string Postcode, string Country) :this( AddressLine1,  AddressLine2,  Town,  County,  Postcode)
        {
            this.Country = Country;
        }

        public Address(Guid AddressId, Guid CustomerId, string AddressLine1, string AddressLine2, string Town, string County, string Postcode, string Country) : this(AddressLine1, AddressLine2, Town, County, Postcode, Country)
        {
            this.AddressId = AddressId;
            this.CustomerId = CustomerId;
        }
    }
}