﻿using System.Collections.Generic;

namespace CustomerAPI.Common
{
    public static class Constants
    {
        public static string AddressNotFound = "Address does not exist in the database!";
        public const string LastCustomerAddress = "Unable to delete Address. Customer must have at least one valid address!";
        public const string CustomerNotFound = "Customer does not exist in the database!";
        public const string DuplicateCustomer = "Customer already exists in the database!";
        public const string DefaultCountry = "UK";
    }
}
