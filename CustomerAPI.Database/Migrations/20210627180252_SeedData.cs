﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace CustomerAPI.Database.Migrations
{
    public partial class SeedData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Customers",
                columns: new[] { "CustomerId", "Email", "Forename", "Mobile", "Surname", "Title" },
                values: new object[] { new Guid("e82681c2-a39e-44db-be55-e40e5503373e"), "frba@mail.com", "Bilbo", "12345678987", "Baggings", "Mr" });

            migrationBuilder.InsertData(
                table: "Customers",
                columns: new[] { "CustomerId", "Email", "Forename", "Mobile", "Surname", "Title" },
                values: new object[] { new Guid("0b214de7-8958-4956-8eed-28f9ba2c47c6"), "brba@mail.com", "Frodo", "12345678987", "Baggings", "Mr" });

            migrationBuilder.InsertData(
                table: "Customers",
                columns: new[] { "CustomerId", "Email", "Forename", "Mobile", "Surname", "Title" },
                values: new object[] { new Guid("3a251d82-e8ce-442f-9e42-5285653a5e8a"), "gagr@mail.com", "Gandalf", "12345678987", "Grey", "Sir" });

            migrationBuilder.InsertData(
                table: "Addresses",
                columns: new[] { "AddressId", "AddressLine1", "AddressLine2", "Country", "County", "CustomerId", "Postcode", "Town" },
                values: new object[] { new Guid("4e579d57-0336-44d7-9a02-f6878ff396b7"), "Baggings", "house", "Middle-Earth", "Central Eriador", new Guid("e82681c2-a39e-44db-be55-e40e5503373e"), "LO TR", "Shire" });

            migrationBuilder.InsertData(
                table: "Addresses",
                columns: new[] { "AddressId", "AddressLine1", "AddressLine2", "Country", "County", "CustomerId", "Postcode", "Town" },
                values: new object[] { new Guid("a0b78a11-43d4-4cd8-88f7-3dbf5b27b5e8"), "Took", "house", "Middle-Earth", "Central Eriador", new Guid("0b214de7-8958-4956-8eed-28f9ba2c47c6"), "LO TR", "Shire" });

            migrationBuilder.InsertData(
                table: "Addresses",
                columns: new[] { "AddressId", "AddressLine1", "AddressLine2", "Country", "County", "CustomerId", "Postcode", "Town" },
                values: new object[] { new Guid("bdc20493-f69c-4bd4-a7d7-a712333f1efc"), "Brandybook", "house", "Middle-Earth", "Central Eriador", new Guid("0b214de7-8958-4956-8eed-28f9ba2c47c6"), "LO TR", "Shire" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Addresses",
                keyColumn: "AddressId",
                keyValue: new Guid("4e579d57-0336-44d7-9a02-f6878ff396b7"));

            migrationBuilder.DeleteData(
                table: "Addresses",
                keyColumn: "AddressId",
                keyValue: new Guid("a0b78a11-43d4-4cd8-88f7-3dbf5b27b5e8"));

            migrationBuilder.DeleteData(
                table: "Addresses",
                keyColumn: "AddressId",
                keyValue: new Guid("bdc20493-f69c-4bd4-a7d7-a712333f1efc"));

            migrationBuilder.DeleteData(
                table: "Customers",
                keyColumn: "CustomerId",
                keyValue: new Guid("3a251d82-e8ce-442f-9e42-5285653a5e8a"));

            migrationBuilder.DeleteData(
                table: "Customers",
                keyColumn: "CustomerId",
                keyValue: new Guid("0b214de7-8958-4956-8eed-28f9ba2c47c6"));

            migrationBuilder.DeleteData(
                table: "Customers",
                keyColumn: "CustomerId",
                keyValue: new Guid("e82681c2-a39e-44db-be55-e40e5503373e"));
        }
    }
}
