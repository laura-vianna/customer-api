﻿using CustomerAPI.Common;
using CustomerAPI.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CustomerAPI.Database
{
    public class AddressDatabaseManager : IAddressDatabaseManager
    {
        public DatabaseContext _dbContext;
        public AddressDatabaseManager(DatabaseContext CustomerDbContext)
        {
            _dbContext = CustomerDbContext;
        }
        public Guid CreateAddress(Customer customer, Address address)
        {

            var newCustomer = AddOrInitialize(customer, address);
            address.Customer = newCustomer;
            _dbContext.Addresses.Add(address);
            _dbContext.Customers.Update(newCustomer);
            _dbContext.SaveChanges();
            return address.AddressId;
        }

        public Address GetAddress(Guid id)
        {
            try
            {
                return _dbContext.Addresses.FirstOrDefault(x => x.AddressId == id);
            }
            catch (ArgumentNullException)
            {
                return null;
            }
        }

        public List<Address> GetAddresses()
        {
            return _dbContext.Addresses.ToList();
        }

        public void DeleteAddress(Guid id)
        {
            var address = GetAddress(id);
            if (address != null)
            {
                _dbContext.Remove(address);
                _dbContext.SaveChanges();
            }
        }

        private Customer AddOrInitialize(Customer customer, Address address)
        {
            if (customer.Addresses == null)
            {
                customer.Addresses = new List<Address>();
            }
            customer.Addresses.Add(address);

            return customer;
        }
    }
}
