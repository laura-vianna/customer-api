﻿using CustomerAPI.Common;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;

namespace CustomerAPI.Database
{
    public class DatabaseContext : DbContext
    {
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Address> Addresses { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite("Data Source=CustomerApi.db;");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Customer>().ToTable("Customers");
            modelBuilder.Entity<Address>().ToTable("Addresses");

            // configures one-to-many relationship
            modelBuilder.Entity<Address>()
                .HasOne(b => b.Customer)
                .WithMany(a => a.Addresses)
                .HasForeignKey(b => b.CustomerId)
                .IsRequired(true);

            var customer1 = new Customer(new Guid("e82681c2-a39e-44db-be55-e40e5503373e"), "Mr", "Bilbo", "Baggings", "frba@mail.com", "12345678987");
            var customer2 = new Customer(new Guid("0b214de7-8958-4956-8eed-28f9ba2c47c6"), "Mr", "Frodo", "Baggings", "brba@mail.com", "12345678987");
            var customer3 = new Customer(new Guid("3a251d82-e8ce-442f-9e42-5285653a5e8a"), "Sir", "Gandalf", "Grey", "gagr@mail.com", "12345678987");

            var address1 = new Address(new Guid("4e579d57-0336-44d7-9a02-f6878ff396b7"), new Guid("e82681c2-a39e-44db-be55-e40e5503373e"), "Baggings", "house", "Shire", "Central Eriador", "LO TR", "Middle-Earth");
            var address2 = new Address(new Guid("a0b78a11-43d4-4cd8-88f7-3dbf5b27b5e8"), new Guid("0b214de7-8958-4956-8eed-28f9ba2c47c6"), "Took", "house", "Shire", "Central Eriador", "LO TR", "Middle-Earth");
            var address3 = new Address(new Guid("bdc20493-f69c-4bd4-a7d7-a712333f1efc"), new Guid("0b214de7-8958-4956-8eed-28f9ba2c47c6"), "Brandybook", "house", "Shire", "Central Eriador", "LO TR", "Middle-Earth");

            modelBuilder.Entity<Customer>().HasData(
                customer1,
                customer2,
                customer3
            );
            modelBuilder.Entity<Address>().HasData(
                address1,
                address2,
                address3
            );
        }
    }
}
