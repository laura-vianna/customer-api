﻿using CustomerAPI.Common;
using CustomerAPI.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CustomerAPI.Database
{
    public class CustomerDatabaseManager : ICustomerDatabaseManager
    {

        public DatabaseContext _dbContext;
        public CustomerDatabaseManager(DatabaseContext CustomerDbContext)
        {
            _dbContext = CustomerDbContext;
        }
        public Guid CreateCustomer(Customer Customer)
        {
            _dbContext.Customers.Add(Customer);
            _dbContext.SaveChanges();
            return Customer.CustomerId;
        }
        public List<Customer> GetCustomers()
        {
            return _dbContext.Customers.Include(i => i.Addresses).ToList();
        }

        public void UpdateCustomer(Customer Customer)
        {
            _dbContext.Customers.Update(Customer);
            _dbContext.SaveChanges();
        }

        public void DeleteCustomer(Guid Id)
        {
            var customer = GetCustomer(Id);
            if (customer != null)
            {
                RemoveOrphanAdresses(customer);
                _dbContext.Remove(customer);
                _dbContext.SaveChanges();
            }
        }

        public Customer GetCustomer(Guid Id)
        {
            try
            {
                return _dbContext.Customers
                    .Include(i => i.Addresses)
                    .FirstOrDefault(x => x.CustomerId == Id);
            }
            catch (ArgumentNullException)
            {
                return null;
            }
        }

        private void RemoveOrphanAdresses(Customer customer)
        {
            if (customer.Addresses != null)
            {
                _dbContext.Addresses.RemoveRange(customer.Addresses.ToArray());
            }
        }
    }
}
