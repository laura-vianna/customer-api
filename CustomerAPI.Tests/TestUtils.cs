﻿using CustomerAPI.Common;
using CustomerAPI.Core;
using CustomerAPI.Database;
using CustomerAPI.Interfaces;
using Moq;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace CustomerAPI.Tests
{
    internal static class TestUtils
    {
        internal static string CreateInvalidField(int fieldLength)
        {
            if (fieldLength == 0)
            {
                return null;
            }
            return new string('*', fieldLength);
        }

        internal static void GetValidaTionResult<T>(T modelToValidate, out bool valid, out ValidationResult firstValidationResult)
        {
            var context = new ValidationContext(modelToValidate);
            var results = new List<ValidationResult>();
            valid = Validator.TryValidateObject(modelToValidate, context, results, true);
            firstValidationResult = results.FirstOrDefault();
        }

        internal static Customer CreateTestCustomer()
        {
            return new Customer("Mr", "Frodo", "Baggings", "frba@mail.com", "12345678987");
        }

        internal static Address CreateTestAddress()
        {
            return new Address("Comfy", "house", "Shire", "Central Eriador", "LO TR", "Middle-Earth");
        }

        internal static Mock<ICustomerDatabaseManager> SetupCustomerDb(Customer getCustomer, List<Customer> allCustomers, List<Customer> currentCustomers)
        {
            var queryable = currentCustomers.AsQueryable();
            var mockDb = new Mock<ICustomerDatabaseManager>();
            var MockContext = new Mock<DatabaseContext>();
            mockDb.Setup(m => m.CreateCustomer(It.IsAny<Customer>()))
                .Callback((Customer customer) => currentCustomers.Add(customer))
                .Returns((Customer customer) => customer.CustomerId);
            mockDb.Setup(m => m.DeleteCustomer(It.IsAny<Guid>()))
                .Callback((Guid id) => RemoveCustomer(id, allCustomers));
            mockDb.Setup(m => m.GetCustomer(It.IsAny<Guid>())).Returns(getCustomer);
            mockDb.Setup(m => m.GetCustomers()).Returns(allCustomers);

            return mockDb;
        }

        internal static Mock<IAddressDatabaseManager> SetupAddressDb(Address getAddress, List<Address> allAddresses, List<Address> currentAddresses)
        {
            var queryable = currentAddresses.AsQueryable();
            var mockDb = new Mock<IAddressDatabaseManager>();
            var MockContext = new Mock<DatabaseContext>();
            mockDb.Setup(m => m.CreateAddress(It.IsAny<Customer>(), It.IsAny<Address>()))
                .Callback((Customer customer, Address address) => AddAddress(customer, address, currentAddresses))
                .Returns((Customer _, Address address) => address.AddressId);
            mockDb.Setup(m => m.DeleteAddress(It.IsAny<Guid>()))
                .Callback((Guid address) => RemoveAddress(address, allAddresses));
            mockDb.Setup(m => m.GetAddress(It.IsAny<Guid>())).Returns(getAddress);
            mockDb.Setup(m => m.GetAddresses()).Returns(allAddresses);            
            return mockDb;
        }

        internal static CustomerManager SetupCustomerManager(Customer getCustomer, List<Customer> allCustomers, List<Customer> currentCustomers)
        {
            Mock<ICustomerDatabaseManager> mockDb = SetupCustomerDb(getCustomer, allCustomers, currentCustomers);
            return new CustomerManager(mockDb.Object);
        }

        internal static AddressManager SetupAddressManager(Customer TestCustomer, Address getAddress, List<Address> allAddresses, List<Address> currentAddresses)
        {
            var customerManager = SetupPopulatedCustomerManager(TestCustomer);
            var mockDb = SetupAddressDb(getAddress, allAddresses, currentAddresses);

            return new AddressManager(mockDb.Object, customerManager);
        }

        private static CustomerManager SetupPopulatedCustomerManager(Customer TestCustomer)
        {
            var allCustomers = new List<Customer> {
                TestCustomer,
                new Customer("Sir", "Gandalf", "Grey", "gagr@mail.com", "12345678987")
            };
            var currentCustomers = new List<Customer>();
            var customerManager = SetupCustomerManager(TestCustomer, allCustomers, currentCustomers);
            return customerManager;
        }
        private static void AddAddress(Customer customer, Address address, List<Address> currentAddresses)
        {
            address.CustomerId = customer.CustomerId;
            currentAddresses.Add(address);
        }

        private static void RemoveCustomer(Guid id, List<Customer> allCustomers)
        {
            var customer = allCustomers.FirstOrDefault(val => val.CustomerId == id);
            RemoveItem(customer, allCustomers);
        }
        private static void RemoveAddress(Guid id, List<Address> allAddresses)
        {
            var address = allAddresses.FirstOrDefault(val => val.AddressId == id);
            RemoveItem(address, allAddresses);
        }

        private static void RemoveItem<T>(T item, List<T> allItems)
        {
            if (allItems != null && item != null)
            {
                allItems.Remove(item);
            }
        }
    }
}
