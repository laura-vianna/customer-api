﻿using CustomerAPI.Common;
using CustomerAPI.Tests;
using System;
using System.Collections.Generic;
using Xunit;

namespace AddressAPI.Tests
{
    public class AddressManagerTests
    {
        private List<Address> currentAddresses;
        private List<Address> allAddresses;
        private Address TestAddress1;
        private Address TestAddress2;
        private Address NewAddress;
        private Customer TestCustomer;



        private void InitializeTest()
        {
            TestAddress1 = TestUtils.CreateTestAddress();
            TestCustomer = TestUtils.CreateTestCustomer();
            TestAddress1.Customer = TestCustomer;

            NewAddress = new Address("Mr", "Bilbo", "Baggings", "frba@mail.com", "12345678987");
            TestAddress2 = new Address("Sir", "Gandalf", "Grey", "gagr@mail.com", "12345678987");
            TestAddress2.Customer = TestCustomer;

            allAddresses = new List<Address> {TestAddress1, TestAddress2};
            TestCustomer.Addresses = allAddresses;

            currentAddresses = new List<Address>();
        }


        [Fact]
        public void ShouldBeAbleToAddCustomerAddress()
        {
            InitializeTest();
            var sut = TestUtils.SetupAddressManager(TestCustomer, TestAddress1, allAddresses, currentAddresses);
            var customerId = Guid.NewGuid();

            var addressId = sut.CreateAddress(NewAddress, customerId);

            Assert.Single(currentAddresses);
            Assert.IsType<Guid>(addressId);
            Assert.NotEqual(addressId, Guid.Empty);
        }


        [Fact]
        public void ShouldBeAbleToRetrieveAddress()
        {
            InitializeTest();
            var sut = TestUtils.SetupAddressManager(TestCustomer, TestAddress1, allAddresses, currentAddresses);

            var result = sut.GetAddressById(TestAddress1.AddressId);

            Assert.Equal(TestAddress1, result);
        }

        [Fact]
        public void ShouldHandleRetrivingNonExistingAddress()
        {
            InitializeTest();
            var sut = TestUtils.SetupAddressManager(TestCustomer, null, allAddresses, currentAddresses);

            void act() => sut.GetAddressById(Guid.NewGuid());

            var exception = Assert.Throws<KeyNotFoundException>(act);
            Assert.Equal(Constants.AddressNotFound, exception.Message);
        }

        [Fact]
        public void ShouldBeAbleToRetrieveAllAddresses()
        {
            InitializeTest();
            var sut = TestUtils.SetupAddressManager(TestCustomer, TestAddress1, allAddresses, currentAddresses);

            var result = sut.GetAllAddresses();

            Assert.Equal(allAddresses, result);
        }

        [Fact]
        public void ShouldBeAbleToRemoveCustomerAddress()
        {
            InitializeTest();
            var sut = TestUtils.SetupAddressManager(TestCustomer, TestAddress1, allAddresses, currentAddresses);

            var initialCount = allAddresses.Count;

            sut.DeleteAddress(TestAddress1.AddressId);

            Assert.Equal(initialCount-1, allAddresses.Count);
        }

        [Fact]
        public void ShouldPreventRemovingLastCustomerAddress()
        {
            InitializeTest();
            var sut = TestUtils.SetupAddressManager(TestCustomer, TestAddress1, allAddresses, currentAddresses);

            var initialCount = allAddresses.Count;

            sut.DeleteAddress(TestAddress1.AddressId);

            void act() => sut.DeleteAddress(TestAddress2.AddressId);

            var exception = Assert.Throws<InvalidOperationException>(act);
            Assert.Equal(Constants.LastCustomerAddress, exception.Message);
            Assert.Equal(initialCount - 1, allAddresses.Count);
        }
    }
}
