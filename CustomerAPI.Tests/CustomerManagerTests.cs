using CustomerAPI.Common;
using System;
using System.Collections.Generic;
using Xunit;

namespace CustomerAPI.Tests
{
    public class CustomerManagerTests
    {
        private List<Customer> currentCustomers;
        private List<Customer> allCustomers;
        private Customer TestCustomer;
        private Customer NewCustomer;


        private void InitializeTest()
        {
            TestCustomer = TestUtils.CreateTestCustomer();
            NewCustomer = new Customer("Mr", "Bilbo", "Baggings", "frba@mail.com", "12345678987");

            allCustomers = new List<Customer> {
                TestCustomer,
                new Customer("Sir", "Gandalf", "Grey", "gagr@mail.com", "12345678987")
            };
            currentCustomers = new List<Customer>();
        }

        [Fact]
        public void ShouldBeAbleToAddCustomer()
        {
            InitializeTest();
            var sut = TestUtils.SetupCustomerManager(TestCustomer, allCustomers, currentCustomers);

            var customerId = sut.CreateCustomer(NewCustomer);

            Assert.Single(currentCustomers);
            Assert.IsType<Guid>(customerId);
            Assert.NotEqual(customerId, Guid.Empty);
        }

        [Fact]
        public void ShouldBeAbleToRetrieveCustomer()
        {
            InitializeTest();
            var sut = TestUtils.SetupCustomerManager(TestCustomer, allCustomers, currentCustomers);

            var result = sut.GetCustomerById(TestCustomer.CustomerId);

            Assert.Equal(TestCustomer, result);
        }

        [Fact]
        public void ShouldHandleRetrivingNonExistingCustomer()
        {
            InitializeTest();
            var sut = TestUtils.SetupCustomerManager(null, allCustomers, currentCustomers);

            void act() => sut.GetCustomerById(Guid.NewGuid());

            var exception = Assert.Throws<KeyNotFoundException>(act);
            Assert.Equal(Constants.CustomerNotFound, exception.Message);
        }

        [Fact]
        public void ShouldBeAbleToRetrieveAllCustomers()
        {
            InitializeTest();
            var sut = TestUtils.SetupCustomerManager(TestCustomer, allCustomers, currentCustomers);

            var result = sut.GetAllCustomers();

            Assert.Equal(allCustomers, result);
        }

        [Fact]
        public void ShouldPreventDuplicateUsersFromBeingAdded()
        {
            InitializeTest();
            var sut = TestUtils.SetupCustomerManager(TestCustomer, allCustomers, currentCustomers);

            void act() => sut.CreateCustomer(TestCustomer);

            var exception = Assert.Throws<InvalidOperationException>(act);
            Assert.Equal(Constants.DuplicateCustomer, exception.Message);
        }

        [Fact]
        public void ShouldBeAbleToRemoveCustomer()
        {
            InitializeTest();
            var sut = TestUtils.SetupCustomerManager(TestCustomer, allCustomers, currentCustomers);
            var initialCount = allCustomers.Count;

            sut.DeleteCustomer(TestCustomer.CustomerId);

            Assert.Equal(initialCount - 1, allCustomers.Count);
        }
    }
}
