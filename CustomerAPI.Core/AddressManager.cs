﻿using CustomerAPI.Common;
using CustomerAPI.Interfaces;
using System;
using System.Collections.Generic;

namespace CustomerAPI.Core
{
    public class AddressManager : IAddressManager
    {
        private readonly ICustomerManager _manager;
        private readonly IAddressDatabaseManager _dbManager;

        public AddressManager(IAddressDatabaseManager dbManager, ICustomerManager manager)
        {
            _dbManager = dbManager;
            _manager = manager;
        }

        public Guid CreateAddress(Address address, Guid CustomerId)
        {
            var customer = _manager.GetCustomerById(CustomerId);

            if (customer != null)
            {
                return _dbManager.CreateAddress(customer, address);
            }
            return Guid.Empty;
        }

        public Address GetAddressById(Guid id)
        {
            var address = _dbManager.GetAddress(id);
            if (address == null)
            {
                throw new KeyNotFoundException(Constants.AddressNotFound);
            }
            return address;
        }

        public List<Address> GetAllAddresses()
        {
            return _dbManager.GetAddresses();
        }

        public void DeleteAddress(Guid addressId)
        {
            var address = _dbManager.GetAddress(addressId);
            var customer = _manager.GetCustomerById(address.CustomerId);

            if (customer != null && customer.Addresses.Count <= 1)
            {
                throw new InvalidOperationException(Constants.LastCustomerAddress);
            }
            _dbManager.DeleteAddress(addressId);
        }
    }
}
