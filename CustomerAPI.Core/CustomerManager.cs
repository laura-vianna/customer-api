﻿using CustomerAPI.Common;
using CustomerAPI.Interfaces;
using System;
using System.Collections.Generic;

namespace CustomerAPI.Core
{
    public class CustomerManager : ICustomerManager
    {
        private readonly ICustomerDatabaseManager _dbManager;
        public CustomerManager(ICustomerDatabaseManager dbManager)
        {
            _dbManager = dbManager;
        }

        public Guid CreateCustomer(Customer customer)
        {
            if (customer != null && CheckCustomerUnique(customer))
            {
                return _dbManager.CreateCustomer(customer);
            }
            return Guid.Empty;
        }

        private bool CheckCustomerUnique(Customer customer)
        {
            var existingCustomers = GetAllCustomers();
            foreach (var existingCustomer in existingCustomers)
            {
                if (existingCustomer.CompareTo(customer))
                {
                    throw new InvalidOperationException(Constants.DuplicateCustomer);
                }
            }
            return true;            
        }

        public Customer GetCustomerById(Guid id)
        {
            var customer = _dbManager.GetCustomer(id);
            if (customer == null)
            {
                throw new KeyNotFoundException(Constants.CustomerNotFound);
            }
            return customer;
        }

        public List<Customer> GetAllCustomers()
        {
            return _dbManager.GetCustomers();
        }

        public void DeleteCustomer(Guid customerId)
        {
            _dbManager.DeleteCustomer(customerId);
        }
    }
}
