﻿using CustomerAPI.Common;
using CustomerAPI.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace CustomerAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class AddressController : ControllerBase
    {
        private readonly IAddressManager _manager;
        public AddressController(IAddressManager manager)
        {
            _manager = manager;
        }

        [HttpPost]
        public IActionResult Create([FromBody] Address address, [FromQuery] Guid customerId)
        {
            try
            {
                var result = _manager.CreateAddress(address, customerId);
                return Created("Get", new { id = result });

            }
            catch (KeyNotFoundException e)
            {
                return NotFound(new { title = e.Message });
            }
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            var addressess = _manager.GetAllAddresses();
            return Ok(addressess);
        }

        [HttpGet("{id}")]
        public IActionResult Get([FromRoute] Guid id)
        {
            try
            {
                var address = _manager.GetAddressById(id);
                return Ok(address);
            }
            catch (KeyNotFoundException e)
            {
                return NotFound(new { title = e.Message });
            }
        }

        [HttpDelete("{id}")]
        public IActionResult Delete([FromRoute] Guid id)
        {
            try
            {
                _manager.DeleteAddress(id);
                return NoContent();
            }
            catch (KeyNotFoundException e)
            {
                return NotFound(new { title = e.Message });
            }
            catch (InvalidOperationException e)
            {
                return BadRequest(new { title = e.Message });
            }
        }
    }
}
