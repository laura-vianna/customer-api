## Prerequisites

- Visual Studio 2019
- .NET Core 5.0
- Docker (linux containers)

### Build
- First time you run this application, initialize the database using `dotnet ef database update --project .\CustomerAPI.Database\`

- Build this application in VS2019 and run using Docker configuration OR

- You can build it using the Docker configuration
```
docker build -f .\CustomerAPI\Dockerfile -t customerapi .
```
- You can run it using the Docker configuration
```
 docker run -dt -e "ASPNETCORE_ENVIRONMENT=Development" -p 49160:443 -p 49165:80 --name customerapicontainer customerapi:latest
```
- The Swagger documentation will be available at `http://localhost:49165/swagger/index.html`

- The application will be available at `http://localhost:49165`
